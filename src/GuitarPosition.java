/**
 * represent a position(string,fret) of a note in guitar.
 * @author sekozer
 *
 */
public class GuitarPosition {
	/**
	 * fret number
	 */
	int fret;
	/**
	 * string number
	 */
	int string;
	public GuitarPosition(int string, int fret){
		this.fret=fret;
		this.string=string;
	}
	public int getFret() {
		return fret;
	}
	public void setFret(int fret) {
		this.fret = fret;
	}
	public int getString() {
		return string;
	}
	public void setString(int string) {
		this.string = string;
	}
	
}
