import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Trainer {
	private Graph g;
	private List<GuitarNode> desiredPath = new ArrayList<GuitarNode>();
	private OptimalFingering optimalFingering;
	private String fileName;
	public Trainer(String fileName, Graph g, int sum, int size) {
		this.g = g;
		
		this.size = size;
		this.sum = sum;
		this.list = new double[size];
		this.result = new double[size];
		this.fileName=fileName;

	}

	public List<GuitarNode> parseDesiredPath(String fileName) {
		desiredPath=new ArrayList<GuitarNode>();
		try {
			Scanner input = new Scanner(new File(fileName));
			while (input.hasNextLine()) {
				Scanner line = new Scanner(input.nextLine());
				int string = line.nextInt();
				int fret = line.nextInt();
				int fingering = line.nextInt();
				GuitarPosition guitarPosition = new GuitarPosition(string, fret);
				GuitarNode guitarNode = new GuitarNode(guitarPosition,
						fingering);
				desiredPath.add(guitarNode);

			}
			return desiredPath;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	private void cut(){
		GuitarNode source = g.getSource();
		GuitarNode dest;
		for(int i=0; i<desiredPath.size(); i++){
			List<Edge> neighbours = g.getNeighbours(source);
			dest = neighbours.get(0).getDestination();
			source = dest;
		}
		List<Edge> neighbours = g.getNeighbours(source);
		for (Edge edge : neighbours) {
			GuitarNode gn = edge.getDestination();
			gn=null;
		}	
	}
	public void train() {
		parseDesiredPath(fileName);
		optimalFingering = new OptimalFingering(g);
		cut();
		comb(sum, size);
		System.out.println("Training result :" + mindif);
		for (int i = 0; i < result.length-1; i++) {
			System.out.print(result[i] + ",");
		}
		System.out.println(result[result.length-1]);
	}

	private int size;
	private int sum;
	private double list[];
	private double result[];
	private int mindif = Integer.MAX_VALUE;

	private int difference(List<GuitarNode> candidatePath) {
		int diff = 0;
		for (int i = 0; i < desiredPath.size(); i++) {
			GuitarNode desiredNode = desiredPath.get(i);
			GuitarNode candidateNode = candidatePath.get(i);
			if (!desiredNode.equal(candidateNode)) {
				diff++;
			}
		}
		return diff;
	}
	int n = 0;
	private void comb(int sum, int count) {
		if (count == 1) {
			list[size - count] = ((double) sum) / this.sum;
			Edge.w = Arrays.copyOf(list, list.length);

			
			List<GuitarNode> candidatePath = optimalFingering
					.DijkstrasAlgortihm();
			n++;
			if(n%1000==0)
				System.out.println(n);
			int diff = difference(candidatePath);
			if (diff < mindif) {
				mindif = diff;
				result = Arrays.copyOf(list, list.length);
			}

		} else {
			for (int i = 1; i < sum; i++) {
				list[size - count] = ((double) i) / this.sum;
				comb(sum - i, count - 1);
			}
		}
	}
}
