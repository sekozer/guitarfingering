import java.util.*;
/**
 * 
 * @author sekozer
 *
 */
public class Graph {
    private Map<GuitarNode, List<Edge>> adjacencyList;  // [vertex] -> [edges]
    private GuitarNode source;
    private List<GuitarNode> destinationLayer;
    /**
     * Sets source node of graph
     * @param source a GuitarNode instance set as source
     */
    public void setSource(GuitarNode source){
    	this.source=source;
    }
    /**
     * Set destination layer of the graph
     * @param destinationLayer
     */
    public void setDestinationLayer(List<GuitarNode> destinationLayer){
    	this.destinationLayer = destinationLayer;
    }
    
    public List<GuitarNode> getDestinationLayer(){
    	return this.destinationLayer;
    }
    
    public GuitarNode getSource (){
    	return source;
    }
    
    public Graph() {
        this.adjacencyList = new HashMap<GuitarNode, List<Edge>>();
    }
    
    public void addVertex(GuitarNode guitarNode) {
        if (guitarNode == null) {
            throw new IllegalArgumentException("null");
        }        
        
        adjacencyList.put(guitarNode, new ArrayList<Edge>());
    }
    
    public Set<GuitarNode> allVertex(){
    	return adjacencyList.keySet();
    }
    
    public List<Edge> getNeighbours(GuitarNode guitarNode){
    	return adjacencyList.get(guitarNode);
    }
    
    /**
     * Adds an edge to graph
     * @param from source of an edge
     * @param to destination of an edge
     */
    public void addEdge(GuitarNode from, GuitarNode to) {
        List<Edge> edgeList = adjacencyList.get(from);
        if (edgeList == null) {
            throw new IllegalArgumentException("source vertex not in graph");
        }
        
        Edge newEdge = new Edge(from, to);
        edgeList.add(newEdge);
    }
    public boolean hasEdge(GuitarNode from, GuitarNode to) {
        return getEdge(from, to) != null;
    }

    public Edge getEdge(GuitarNode from, GuitarNode to) {
        List<Edge> edgeList = adjacencyList.get(from);
        if (edgeList == null) {
            throw new IllegalArgumentException("source vertex not in graph");
        }
        
        for(Edge e : edgeList) {
        	if (e.getDestination().equals(to)) {
        		return e;
        	}
        }
        
        return null;
    }

    
    public String toString() {
        Set<GuitarNode> keys = adjacencyList.keySet();
        String str = "";
        
        for (GuitarNode v : keys) {
        	if(v!=null){
            str += v.getDotString() + ": ";
            
            List<Edge> edgeList = adjacencyList.get(v);
            
            for (Edge edge : edgeList) {
                str += edge.getDotString() + "  ";
            }
            str += "\n";
        	}
        }
        return str;
    }
    /**
     * @return  dot string of a graph
     */
    public String toDotString(){
        Set<GuitarNode> keys = adjacencyList.keySet();
        String dotGraph = "graph g{" + "\n";
        
        for (GuitarNode v : keys) {
        	List<Edge> edgeList = adjacencyList.get(v);
        	if(v.getFingeringNumber()==0)
        		System.out.println();
            for (Edge edge : edgeList) 
            	dotGraph+= edge.getDotString();

        }
        dotGraph += "}";
        return dotGraph;
    }
    /**
     * 
     * @return compact .dot string of a graph
     */
    public String toCompactDotString(){
        String dotGraph = "graph g{" + "\n";
        GuitarNode source = this.source;
        List<GuitarNode> leftLayer = new ArrayList<GuitarNode>();
        leftLayer.add(source);
        List<GuitarNode> rightLayer = getEndNodes(adjacencyList.get(source));
        while(!rightLayer.isEmpty()){
        	dotGraph+= getEdgeString(leftLayer, rightLayer);
        	leftLayer=rightLayer;
        	rightLayer=getEndNodes(adjacencyList.get(rightLayer.get(0)));
        }
        
        dotGraph += "}";
        return dotGraph;
    }
    private List<GuitarNode> getEndNodes(List<Edge> edges){
    	List<GuitarNode> endNodes = new ArrayList<GuitarNode>();
    	for (Edge edge : edges) {
			endNodes.add(edge.getDestination());
		}
    	return endNodes;
    }
	private String getEdgeString(List<GuitarNode> leftLayer,
			List<GuitarNode> rightLayer) {
		String result = "{";
		for (GuitarNode guitarNodeLeft : leftLayer) {
			result += " " + guitarNodeLeft.getDotString();
		}
		result += "}";
		result += " -- ";
		result += "{";
		for (GuitarNode guitarNodeRight : rightLayer) {
			result += " " + guitarNodeRight.getDotString();
		}
		result += "}";
		return result;
	}

    
    
   
   
}