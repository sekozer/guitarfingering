import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class TabRenderer extends Renderer {

	private String[] openStringNames = { "E", "B", "G", "D", "A", "E" };

	public TabRenderer(Score s, List<GuitarNode> guitarNodes) {
		super(s, guitarNodes);

	}

	public void printDash(int n) {
		for (int i = 0; i < n; i++) {
			System.out.print("-");
		}
	}
	public void printSpace(int n,PrintWriter writer) {
		for (int i = 0; i < n; i++) {
			writer.write(" ");
		}
	}
	public void printSpace(int n) {
		for (int i = 0; i < n; i++) {
			System.out.print(" ");
		}
	}



	public void render(int mod) throws FileNotFoundException {
		PrintWriter writer = new PrintWriter("tab.txt");

		List<Measure> measures = score.constructMeasures();
		
		for (int i = 0; i < measures.size(); i += mod) {
			int innerMeasuresLen;
			if (i + mod > measures.size())
				innerMeasuresLen = measures.size() % mod;
			else
				innerMeasuresLen = mod;
			Measure innerMeasures[] = new Measure[innerMeasuresLen];
			for (int j = 0; j < innerMeasuresLen; j++) {
				innerMeasures[j] = measures.get(i + j);
			}
			List<List<Note>> notesList = new ArrayList<List<Note>>();
			for (int j = 0; j < innerMeasures.length; j++) {
				notesList.add(innerMeasures[j].getNotes());
			}
			System.out.println(i);
			writer.write(i+"\r\n");
			for (int string = 1; string <= 6; string++) {
				for (int j = 0; j < innerMeasures.length; j++) {
					if (j % mod == 0){
						System.out.print(openStringNames[string - 1] + "|");
						writer.write(openStringNames[string - 1] + "|");
					}
					List<Note> notes = notesList.get(j);
					for (Note note : notes) {

						if (!note.isRest() && !note.isGhost) {
							GuitarNode guitarNode = noteFingeringMap.get(note);
							GuitarPosition guitarPosition = guitarNode
									.getGuitarPosition();
							if (guitarPosition.getString() != string) {
								System.out.print("-");
								writer.write("-");
								if (guitarPosition.getFret() > 9){
									System.out.print("-");
									writer.write("-");
								}
							} else {
								
								writer.write(""+guitarPosition.getFret());
								System.out.print(guitarPosition.getFret());
							}
						}
						printDash(note.getTabWidth());
						for (int k = 0; k < note.getTabWidth(); k++) {
							writer.write("-");
						}
					}
					System.out.print("|");
					writer.write("|");
				}
				writer.write("\r\n");
				System.out.println();

			}
			System.out.print("L ");
			writer.write("L ");
			for (int j = 0; j < innerMeasures.length; j++) {
				List<Note> notes = notesList.get(j);

				for (Note note : notes) {
					if (!note.isRest() && !note.isGhost) {
						GuitarNode guitarNode = noteFingeringMap.get(note);
						GuitarPosition guitarPosition = guitarNode
								.getGuitarPosition();
						writer.write(""+guitarNode.getFingeringNumber());
						System.out.print(guitarNode.getFingeringNumber());
						if (guitarPosition.getFret() > 9){
							System.out.print(" ");
							writer.write(" ");
						}
					}
					
					printSpace(note.getTabWidth());
					printSpace(note.getTabWidth(),writer);
				}
				System.out.print(" ");
				writer.write(" ");
			}
			writer.write("\r\n");
			System.out.println();

		}
		writer.write("\r\n");
		System.out.println();
		writer.close();
	}



}
