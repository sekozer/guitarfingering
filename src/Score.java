import java.util.ArrayList;
import java.util.List;


public class Score {
	private int keySignature;
	private int numerator;
	private int denominator;
	private String MajMin;
	private List<Note> notes=new ArrayList<Note>();
	private List<Measure> measures = null;
	public long poly=-1;
	public long noteons=-1;
	public void setMeasures(List<Measure> measures){
		this.measures = measures;
	}
	public List<Measure> constructMeasures(){
		
		List<Measure> measures = new ArrayList<Measure>();
		int measureSum=numerator*(32/denominator);
		long temp=0;
		List<Note> tempNotes = new ArrayList<Note>();

		for(int i=0; i<notes.size(); i++){

			Note n=notes.get(i);
			long duration = n.getDuration();
			temp+=duration;
			if(temp==measureSum){
				Measure m = new Measure();
				tempNotes.add(n);
				m.setNotes(tempNotes);
				measures.add(m);
				temp=0;
				tempNotes = new  ArrayList<Note>();
			}
			else if(temp>measureSum){
				/*
				 * Temp duration is larger than a measure. 
				 */
				
				long prevTemp = temp-duration;
				long part = measureSum-prevTemp;
				//Complete measure with this, part is measure duration minus previous temp.
				n.duration = part;
				tempNotes.add(n);
				Measure m = new Measure();
				m.setNotes(tempNotes);
				measures.add(m);
				//Now handle remaining part
				temp = temp - measureSum;
				
				/*
				 * Create Ghost measures to complete remaining part
				 * ex: temp = 15 measureSum 4  create three ghost measure.
				 */
				long ghostNoteNumber = temp/measureSum;
				for(int j=0; j<ghostNoteNumber; j++){
					tempNotes  = new ArrayList<Note>();
					Note remainingNote = new Note(measureSum,n.octave,n.step);
					remainingNote.isGhost = true;
					tempNotes.add(remainingNote);
					Measure ghost = new Measure();
					ghost.setNotes(tempNotes);
					measures.add(m);
				}
				// 15%4=3 create ghost duration for this
				tempNotes  = new ArrayList<Note>();
				temp = temp % measureSum;
				if(temp!=0){
					Note remainingNote = new Note(temp, n.octave, n.step);
					remainingNote.isGhost=true;
					tempNotes.add(remainingNote);
				}
			}
			else{
				tempNotes.add(n);
			}
			
		}
		if(!tempNotes.isEmpty()){
			Measure m = new Measure();
			m.setNotes(tempNotes);
			measures.add(m);
		}
		return measures;
	}
	public void addMeasure(Measure m){
		measures.add(m);
	}
	public void addNote(Note n){
		notes.add(n);

	}
	public int getKeySignature() {
		return keySignature;
	}
	public List<Measure> getMeasures(){
		if(measures==null)
			measures = constructMeasures();
		return measures;
	}
	public void setKeySignature(int keySignature) {
		this.keySignature = keySignature;
	}
	public int getNumerator() {
		return numerator;
	}
	public void setNumerator(int numerator) {
		this.numerator = numerator;
	}
	public int getDenominator() {
		return denominator;
	}
	public void setDenominator(int denominator) {
		this.denominator = denominator;
	}
	public String getMajMin() {
		return MajMin;
	}
	public void setMajMin(String majMin) {
		MajMin = majMin;
	}
	public List<Note> getNotes() {
		return notes;
	}
	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}

}
