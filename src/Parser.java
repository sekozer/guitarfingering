import java.util.ArrayList;
import java.util.List;

public abstract class Parser {
	public Parser(String fileName) {
		this.fileName = fileName;

	}

	String fileName;

	public static final String[] NOTE_NAMES = { "C", "C#", "D", "D#", "E", "F",
			"F#", "G", "G#", "A", "A#", "B" };

	public static final int openStringMidiNotes[] = { 40, 45, 50, 55, 59, 64 };
	public static final int openStringMidiNotesShifted[] = { 52, 57, 62, 67, 71, 76 };
	private int activeMidiNotes[]=new int[6];
	public GuitarPosition getLowestPositionFromMidiNote(int midiNote) {
		int string;
		int i;
		if (midiNote < 40 || midiNote > activeMidiNotes[5]+17)
			return null;
		for (i = activeMidiNotes.length - 1; i >= 0; i--) {
			if (midiNote >= activeMidiNotes[i])
				break;

		}
		string = 6 - i;
		int fret = midiNote - activeMidiNotes[i];
		GuitarPosition guitarPosition = new GuitarPosition(string, fret);

		return guitarPosition;

	}

	private List<GuitarPosition> getGuitarPositions(int midiNote) {
		GuitarPosition guitarPosition = getLowestPositionFromMidiNote(midiNote);
		if (guitarPosition == null)
			return null;
		List<GuitarPosition> fretPositions = new ArrayList<GuitarPosition>();
		int string = guitarPosition.getString();
		int fret = guitarPosition.getFret();
		while (fret <= 19 && string <= 6) {
			fretPositions.add(new GuitarPosition(string, fret));
			if (string == 2) {
				// Fret difference between G and B is 4.
				fret += 4;
			} else {
				fret += 5;
			}
			string += 1;
		}
		return fretPositions;
	}

	private List<GuitarNode> getGuitarNodesFromPositions(
			List<GuitarPosition> guitarPositions) {
		if (guitarPositions == null)
			return null;
		List<GuitarNode> guitarNodes = new ArrayList<GuitarNode>();
		for (GuitarPosition guitarPosition : guitarPositions)
			guitarNodes.addAll(GenerateNodesForGuitarPosition(guitarPosition));
		return guitarNodes;

	}

	private List<GuitarNode> GenerateNodesForGuitarPosition(
			GuitarPosition guitarPosition) {
		List<GuitarNode> guitarNodes = new ArrayList<GuitarNode>();
		if (guitarPosition.getFret() == 0) {
			GuitarNode guitarNode = new GuitarNode(guitarPosition, 0);
			guitarNodes.add(guitarNode);
		} else {
			for (int i = 1; i <= 4; i++)
				guitarNodes.add(new GuitarNode(guitarPosition, i));
		}
		return guitarNodes;
	}

	int index = 1;

	private List<Edge> generateEdges(List<GuitarNode> leftLayer,
			List<GuitarNode> rightLayer, Graph g) {
		List<Edge> edges = new ArrayList<Edge>();
		index++;
		for (GuitarNode guitarNodeLeft : leftLayer) {
			g.addVertex(guitarNodeLeft);
			for (GuitarNode guitarNodeRight : rightLayer) {
				guitarNodeRight.setLayerIndex(index);
				g.addVertex(guitarNodeRight);
				edges.add(new Edge(guitarNodeLeft, guitarNodeRight));
				g.addEdge(guitarNodeLeft, guitarNodeRight);
			}

		}

		return edges;
	}
	public void rangeCheck(List<Integer> midiNotes){
		int min = Integer.MAX_VALUE;
		for (Integer midiNote : midiNotes) {
			if(midiNote<min)
				min = midiNote;
		}
		if(min>=52){
			activeMidiNotes = openStringMidiNotesShifted;
		}
		else if(min>=40){
			activeMidiNotes = openStringMidiNotes;
		}
	}
	public Graph getGraph() {
		List<Integer> midiNotes = getNotes();
		rangeCheck(midiNotes);
		GuitarPosition gp = new GuitarPosition(0, 0);
		GuitarNode source = new GuitarNode(gp, 0);
		List<GuitarNode> previousGuitarNodes = new ArrayList<GuitarNode>();
		previousGuitarNodes.add(source);
		Graph g = new Graph();
		g.setSource(source);
		List<GuitarNode> guitarNodes = getGuitarNodesFromPositions(getGuitarPositions(midiNotes
				.get(0)));
		g.addVertex(source);
		for (GuitarNode guitarNode : guitarNodes) {
			guitarNode.setLayerIndex(1);
			g.addEdge(source, guitarNode);
		}
		previousGuitarNodes = guitarNodes;
		for (int i = 1; i < midiNotes.size(); i++) {
			int midiNote = midiNotes.get(i);
			List<GuitarPosition> guitarPositions = getGuitarPositions(midiNote);
			if (guitarPositions == null)
				continue;
			guitarNodes = getGuitarNodesFromPositions(guitarPositions);
			generateEdges(previousGuitarNodes, guitarNodes, g);
			previousGuitarNodes = guitarNodes;
		}

		g.setDestinationLayer(previousGuitarNodes);
		return g;

	}

	public abstract List<Integer> getNotes();

	public abstract Score getScore();

}
