import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class MusicXMLRenderer extends Renderer {

	public MusicXMLRenderer(Score s, List<GuitarNode> guitarNodes) {
		super(s, guitarNodes);
		factory = DocumentBuilderFactory.newInstance();
		try {
			docBuilder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		doc = docBuilder.newDocument();
		root = doc.createElement("score-partwise");
		List<Note> notes = s.getNotes();
		Note minNote = notes.get(0);
		for (int i = 1; i < notes.size(); i++) {
			Note n = notes.get(i);
			if (n.duration < minNote.getDuration())
				minNote = n;
		}
		double minLength = minNote.getQuarterLength();
		if (minLength < 1) // there is some note smaller than a quarter then
			divisions = Math.round(1 / minNote.getQuarterLength());
		doc.appendChild(root);
		root.appendChild(getPartList());
		root.appendChild(getPart());

	}

	private Element root;
	private DocumentBuilderFactory factory;
	private DocumentBuilder docBuilder;
	private Document doc;

	public void writeToFile(String filename) {
		try {
			Transformer tr = TransformerFactory.newInstance().newTransformer();
			tr.setOutputProperty(OutputKeys.INDENT, "yes");
			tr.setOutputProperty(OutputKeys.METHOD, "xml");
			tr.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,
					"-//Recordare//DTD MusicXML 2.0 Partwise//EN");
			tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			tr.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,
					"http://www.musicxml.org/dtds/partwise.dtd");
			tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount",
					"4");

			// send DOM to file
			FileOutputStream f = new FileOutputStream(filename);
			tr.transform(new DOMSource(doc), new StreamResult(f));

		} catch (TransformerException te) {
			System.out.println(te.getMessage());
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		}
	}

	private Element getPartList() {
		Element partlist = doc.createElement("part-list");
		partlist.appendChild(getScorePart());
		return partlist;
	}

	private Element getScorePart() {
		Element part = doc.createElement("score-part");
		Attr attr = doc.createAttribute("id");
		attr.setValue("P1");
		part.setAttributeNode(attr);
		Element partname = doc.createElement("part-name");
		Text partnameString = doc.createTextNode("Music");
		partname.appendChild(partnameString);
		return part;
	}

	private Element getPart() {
		Element part = doc.createElement("part");
		Attr attr = doc.createAttribute("id");
		attr.setValue("P1");
		part.setAttributeNode(attr);

		System.out.println();
		List<Measure> measures = score.getMeasures();

		System.out.println(" ");
		for (int i = 0; i < measures.size(); i++) {
			Measure previous, next;
			Note lastPrevious, firstNext;
			if (i == 0)
				lastPrevious = null;
			else {
				previous = measures.get(i - 1);
				lastPrevious = previous.notes.get(previous.notes.size() - 1);
			}
			if (i == measures.size() - 1)
				firstNext = null;
			else {
				next = measures.get(i + 1);
				firstNext = next.notes.get(0);
			}
			part.appendChild(getMeasure(measures.get(i), lastPrevious,
					firstNext, i));
		}

		return part;

	}

	int counter = 0;

	private Note lastPrevious, lastNext;

	private Element getMeasure(Measure measure, Note lastPrevious,
			Note lastNext, int id) {
		Element measureElement = doc.createElement("measure");

		addMeasureNumber(measureElement, id + 1);
		if (id == 0)
			measureElement.appendChild(getAttributes(measure));
		List<Note> notes = measure.notes;
		for (int i = 0; i < notes.size(); i++) {
			Note note = notes.get(i);
			Note previousNote, nextNote;
			if (i == 0)
				previousNote = lastPrevious;
			else
				previousNote = notes.get(i - 1);

			if (i < notes.size() - 1)
				nextNote = notes.get(i + 1);
			else
				nextNote = lastNext;
			GuitarNode guitarNode = noteFingeringMap.get(note);
			boolean isFingeringAvail = guitarNode != null;
		

			if(isFingeringAvail&&!note.isGhost)
			measureElement.appendChild(getFingeringDirection(note));
			measureElement.appendChild(getNoteElement(note, previousNote,
					nextNote));

		}
		return measureElement;
	}

	private void addMeasureNumber(Element measure, int mNum) {
		Attr attr = doc.createAttribute("number");
		attr.setValue(Integer.toString(mNum));
		measure.setAttributeNode(attr);
	}

	private Element getAttributes(Measure m) {
		Element attributes = doc.createElement("attributes");
		attributes.appendChild(getDivisions(m.getDivisions()));
		attributes.appendChild(getKey(m.getFifths(), m.getMode()));
		attributes.appendChild(getTime(m.getBeats(), m.getBeatType()));
		attributes.appendChild(getClef(m.getSign(), m.getLine()));
		return attributes;
	}

	private Element getDivisions(int division) {
		Element divisions = doc.createElement("divisions");
		Text divisionString = doc.createTextNode(this.divisions + "");
		divisions.appendChild(divisionString);

		return divisions;
	}

	private Element getKey(int fifths, String mode) {
		Element key = doc.createElement("key");
		Element fifthsElement = doc.createElement("fifths");
		if (fifths == -1)
			addAttribute(fifthsElement,
					Integer.toString(score.getKeySignature()));
		else
			addAttribute(fifthsElement, Integer.toString(fifths));
		Element modeElement = doc.createElement("mode");
		if (mode.equals(""))
			addAttribute(modeElement, score.getMajMin());
		else
			addAttribute(modeElement, mode);
		key.appendChild(fifthsElement);
		key.appendChild(modeElement);
		return key;
	}

	private Element getTime(int beats, int beatType) {
		Element time = doc.createElement("time");
		Element beatsElement = doc.createElement("beats");
		if (beats == -1)
			addAttribute(beatsElement, Integer.toString(score.getDenominator()));
		else
			addAttribute(beatsElement, Integer.toString(beats));
		Element beatTypeElement = doc.createElement("beat-type");
		if (beatType == -1)
			addAttribute(beatTypeElement,
					Integer.toString(score.getNumerator()));
		else
			addAttribute(beatTypeElement, Integer.toString(beatType));
		time.appendChild(beatsElement);
		time.appendChild(beatTypeElement);
		return time;
	}

	private void addAttribute(Element e, String s) {
		e.appendChild(doc.createTextNode(s));
	}

	private Element getClef(String sign, int clef) {
		Element clefElement = doc.createElement("clef");
		Element signElement = doc.createElement("sign");
		addAttribute(signElement, "G");
		Element lineElement = doc.createElement("line");
		addAttribute(lineElement, "2");
		clefElement.appendChild(signElement);
		clefElement.appendChild(lineElement);
		return clefElement;
	}

	private Element getNoteElement(Note n, Note previous, Note next) {
		Element note = doc.createElement("note");
		if (n.isRest()) {
			note.appendChild(getRestElement());
			note.appendChild(getDurationElement(n.getQuarterLength()));
			String rtype = n.getType();
			if (rtype != null)
				note.appendChild(getTypeElement(rtype));
		} else if (n.isGhost) {
			if (previous != null) {
				GuitarNode guitarNode = noteFingeringMap.get(previous);
				note.appendChild(getPitchElement(guitarNode.getMidiNote()));
				note.appendChild(getDurationElement(n.getQuarterLength()));
				note.appendChild(getTie(false));
				Element notations = getNotations();
				notations.appendChild(getTied(false));
				note.appendChild(notations);
				String type = n.getType();
				if (type != null) {
					note.appendChild(getTypeElement(type));
				}
			}
		} else {

			GuitarNode guitarNode = noteFingeringMap.get(n);
			if (guitarNode != null)
				note.appendChild(getPitchElement(guitarNode.getMidiNote()));
			note.appendChild(getDurationElement(n.getQuarterLength()));
			if (isDotted(divisions, n.duration))
				note.appendChild(getDotElement());
			String type = n.getType();
			if (type != null) {
				note.appendChild(getTypeElement(type));
			}
			boolean isNextGhost = next != null && next.isGhost;
			boolean isFingeringAvail = guitarNode != null;
			Element notations = getNotations();
			if (isNextGhost) {
				note.appendChild(getTie(true));
				notations.appendChild(getTied(true));
			}
			/*
			if (isFingeringAvail)
				notations.appendChild(getFingering(n));
				*/
			if (isFingeringAvail || isNextGhost) {
				note.appendChild(notations);
			}
		}
		return note;
	}

	private boolean isDotted(long divisions, long duration) {
		if (divisions > duration && divisions % duration != 0)
			return true;
		else if (divisions < duration) {
			if (duration % divisions == 0) {
				if ((duration / divisions) % 2 == 0)
					return false;
				else
					return true;
			} else {
				return true;
			}
		} else
			return false;
	}

	private Element getDotElement() {
		Element rest = doc.createElement("dot");
		return rest;
	}

	private Element getRestElement() {
		Element rest = doc.createElement("rest");
		return rest;
	}

	private Element getPitchElement(int noteNumber) {
		Element pitch = doc.createElement("pitch");
		int key = noteNumber % 12;
		pitch.appendChild(getStepElement(key));
		if (Note.alter(key)) {
			pitch.appendChild(getAlterElement());

		}
		;
		pitch.appendChild(getOctaveElement(noteNumber / 12));
		return pitch;
	}

	private Element getOctaveElement(int octaveNum) {
		Element octave = doc.createElement("octave");
		Text octaveString = doc.createTextNode("" + octaveNum);
		octave.appendChild(octaveString);
		return octave;
	}

	private Element getStepElement(int stepNum) {
		Element step = doc.createElement("step");
		Text stepString = doc.createTextNode("" + Note.getLetter(stepNum));
		step.appendChild(stepString);
		return step;
	}

	private Element getAlterElement() {

		Element alter = doc.createElement("alter");
		Text alterString = doc.createTextNode("" + 1);
		alter.appendChild(alterString);
		return alter;
	}

	long divisions = 1;

	private Element getDurationElement(double quarterLength) {
		Element durationElement = doc.createElement("duration");
		long duration = Math.round(quarterLength * divisions);

		Text durationString = doc.createTextNode(Long.toString(duration));
		durationElement.appendChild(durationString);
		return durationElement;
	}

	private Element getNotations() {
		Element notations = doc.createElement("notations");

		return notations;
	}

	private Element getTied(boolean isStart) {
		Element tie = doc.createElement("tied");
		Attr attr = doc.createAttribute("type");
		if (isStart)
			attr.setValue("start");
		else
			attr.setValue("stop");
		tie.setAttributeNode(attr);
		return tie;
	}

	private Element getTie(boolean isStart) {
		Element tie = doc.createElement("tie");
		Attr attr = doc.createAttribute("type");
		if (isStart)
			attr.setValue("start");
		else
			attr.setValue("stop");
		tie.setAttributeNode(attr);
		return tie;
	}

	/*
	 * <direction placement="above"> <direction-type> <words default-x="15"
	 * default-y="15" font-size="9" font-style="italic">1</words>
	 * </direction-type> </direction>
	 */
	private Element getFingeringDirection(Note note){
		GuitarNode guitarNode = noteFingeringMap.get(note);
		Element direction = doc.createElement("direction");
		Attr attrPlacement = doc.createAttribute("placement");
		attrPlacement.setValue("above");
		direction.setAttributeNode(attrPlacement);
		Element directionType = doc.createElement("direction-type");
		Element words = doc.createElement("words");
		Attr attrFont = doc.createAttribute("font-size");
		attrFont.setValue("9");

		Attr attrDefy = doc.createAttribute("relative-y");
		attrDefy.setValue("3");
		words.setAttributeNode(attrFont);
		words.setAttributeNode(attrDefy);
		Text fingeringString = doc.createTextNode(""
				+ guitarNode.getFingeringNumber());
		words.appendChild(fingeringString);
		directionType.appendChild(words);
		direction.appendChild(directionType);

		return direction;
	}

	private Element getFingering(Note note) {
		GuitarNode guitarNode = noteFingeringMap.get(note);
		Element technical = doc.createElement("technical");
		Element fingering = doc.createElement("fingering");
		Text fingeringString = doc.createTextNode(""
				+ guitarNode.getFingeringNumber());
		fingering.appendChild(fingeringString);
		technical.appendChild(fingering);
		return technical;

	}

	private Element getTypeElement(String type) {
		Element typeElement = doc.createElement("type");
		Text typeString = doc.createTextNode(type);
		typeElement.appendChild(typeString);
		return typeElement;
	}

}
