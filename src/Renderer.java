import java.util.HashMap;
import java.util.List;
import java.util.Map;


public abstract class Renderer {
	protected Score score;
	protected List<GuitarNode> guitarNodes;
	protected Map<Note,GuitarNode> noteFingeringMap = new HashMap<Note,GuitarNode>();
	
	public Renderer(Score s,List<GuitarNode> guitarNodes){
		this.score=s;
		this.guitarNodes=guitarNodes;
		if(s!=null)matchFingerings();
	}
	public void matchFingerings(){
		List<Note> notes= score.getNotes();
		int counter = 0;
		for (Note note : notes) {
			if(!note.isRest()&&!note.isGhost){
				noteFingeringMap.put(note, guitarNodes.get(counter));
				counter++;
			}
		}
	}

}
