import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**	
 * GuitarFingeringMain.java
 * main function for handling parsers, fingering algorithm and renderers
 * @author Serkan Ozer
 * @version 1.0
 */
public class GuitarFingeringMain {

	public static void main(String args[]) throws IOException {
		MidiParser midiParser = new MidiParser(
				"./test/Carcassi_Etude_No8_Op60.mid");
		//MusicXMLParser mparse = new MusicXMLParser("musout.xml");
		//MusicXMLParser m = new MusicXMLParser("./test/mus.xml");
		//0.25, 0.25, 0.15, 0.15, 0.05 ,0.15 favors lower frets
		//0.25, 0.25 ,0.1, 0.2, 0.05 ,0.15 favors lower frets
		//0.45, 0.1, 0.05, 0.05 ,0.15, 0.2 favors higher frets
		double weights[] = {0.25, 0.25 ,0.1, 0.2, 0.05 ,0.15};
		Edge.w = weights;
		Graph g = midiParser.getGraph();
		Score s = midiParser.getScore();
		OptimalFingering opFingering = new OptimalFingering(g);
		List<GuitarNode> l = opFingering.DijkstrasAlgortihm();
		PrintWriter out = new PrintWriter("output.dot");
		out.print(g.toCompactDotString());
		out.close();
		MusicXMLRenderer musicXMLrenderer = new MusicXMLRenderer(s, l);
		musicXMLrenderer.writeToFile("musout.xml");
		TabRenderer tabRenderer = new TabRenderer(s, l);
		tabRenderer.render(2);

		/*
		Trainer trainer = new Trainer("./test/longlower.txt", g, 20, 6);
		trainer.train();
		List<GuitarNode> desiredPath = trainer.parseDesiredPath("./test/longlower.txt");
		*/

	
				

	}

}
