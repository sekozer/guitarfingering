/**	
 * Edge.java
 * purpose: represents a transition from one guitar fingering to another. Cost functions are also implemented here
 * @author Serkan Ozer
 * @version 1.0
 */
public class Edge {
	
	/** Source of an edge */
	private GuitarNode guitarNodeSource;
	/** Destination of an edge */
	private GuitarNode guitarNodeDestination;
	/** Total cost of an edge */
	public double cost;
	/** Cost array for finger pairs with finger difference of 1 such as index-middle, middle-ring.*/
	public static double[] costArrayD1 = { 5.0, 1.5, 0.25, 2.75 };
	/** Cost array for finger pairs with finger difference of 2 such as index-ring, middle-little.*/
	public static double[] costArrayD2 = { 5.0, 3.0, 1.5, 0.25, 2.3 };
	/** Cost array for finger pairs with finger difference of 3 such as index-little.*/
	public static double[] costArrayD3 = { 5.0, 4.0, 3.0, 1.5, 0.25, 2.0 };
	/** Cost of sliding finger*/
	public static double slidingCost = 5;
	/** weight vector of costs*/
	public static double[] w = { 1, 1, 1, 1, 1, 1 };
	/**
	 * 
	 * @param guitarNodeSource source of an edge
	 * @param guitarNodeDestination sourca of a destination
	 */
	public Edge(GuitarNode guitarNodeSource, GuitarNode guitarNodeDestination) {
		this.guitarNodeSource = guitarNodeSource;
		this.guitarNodeDestination = guitarNodeDestination;
		this.cost = w[0] * f0() + w[1] * f1() + w[2] * f2() + w[3] * f3()
				+ w[4] * f4() + w[5] * f5();
	}

	/**
	 * 
	 * @return .dot file representation of an edge.
	 */
	public String getDotString() {
		return "\t" + guitarNodeSource.getDotString() + " -- "
				+ guitarNodeDestination.getDotString() + "[ label=\"" + cost
				+ "\" ]" + ";\n";
	}
	/**
	 * 
	 * @return cost of an edge
	 */
	public double getCost() {
		return w[0] * f0() + w[1] * f1() + w[2] * f2() + w[3] * f3()
				+ w[4] * f4() + w[5] * f5();
	}
	/**
	 * 
	 */
	public String toString() {
		return "\t" + guitarNodeSource.getDotString() + " -- "
				+ guitarNodeDestination.getDotString() + "[ label=\"" + cost
				+ "\" ]" + ";\n";
	}
	/**
	 * 
	 * @return destination edge of this edge
	 */
	public GuitarNode getDestination() {
		return this.guitarNodeDestination;
	}
	/**
	 *  given two fingering position i and j this cost is a function of fret difference, finger i and finger j.
	 *  Depending on fret difference and playing fingers i and j corresponding costs are pulled from 
	 *  costArrayD1,costArrayD2,costArrayD3,costArrayD4,costArrayD5
	 * @return fret stretch cost of this edge
	 */
	public double f0() { // fret stretch
		int fretDestination = guitarNodeDestination.getGuitarPosition()
				.getFret();
		int fretSource = guitarNodeSource.getGuitarPosition().getFret();
		if (fretDestination == 0 || fretSource == 0)
			return 0;
		int deltaFret = fretDestination - fretSource;
		int deltaFinger = guitarNodeDestination.getFingeringNumber()
				- guitarNodeSource.getFingeringNumber();
		/*
		 * index-middle, middle-little,little-pinky
		 */
		if (deltaFinger == 1) {
			// give high cost if you play fret 3 to 2 finger middle to index
			if (deltaFret < 0) {
				return costArrayD1[0];
			} else if (deltaFret > 2) {
				return costArrayD1[costArrayD1.length - 1];
			} else {
				return costArrayD1[deltaFret + 1];
			}
		} /*
		 * middle-index etc.
		 */
		else if (deltaFinger == -1) {
			if (deltaFret > 0) {
				return costArrayD1[0];
			} else if (deltaFret < -2) {
				return costArrayD1[costArrayD1.length - 1];
			} else {
				if (deltaFret == 0)
					return 3.5;
				return costArrayD1[-deltaFret + 1];
			}
		} else if (deltaFinger == 2) {
			if (deltaFret < 0) {
				return costArrayD2[0];
			} else if (deltaFret > 3) {
				return costArrayD2[costArrayD2.length - 1];
			} else {
				return costArrayD2[deltaFret + 1];
			}
		} else if (deltaFinger == -2) {
			if (deltaFret > 0) {
				return costArrayD2[0];
			} else if (deltaFret < -3) {
				return costArrayD2[costArrayD2.length - 1];
			} else {
				if (deltaFret == 0)
					return 4;
				return costArrayD2[-deltaFret + 1];
			}
		} else if (deltaFinger == 3) {
			if (deltaFret < 0) {
				return costArrayD3[0];
			} else if (deltaFret > 4) {
				return costArrayD3[costArrayD3.length - 1];
			} else {
				return costArrayD3[deltaFret + 1];
			}
		} else if (deltaFinger == -3) {
			if (deltaFret > 0) {
				return costArrayD3[0];
			} else if (deltaFret < -4) {
				return costArrayD3[costArrayD3.length - 1];
			} else {
				if (deltaFret == 0)
					return 4.75;
				else if (deltaFret == -1)
					return 4.5;
				return costArrayD3[-deltaFret + 1];
			}
		}

		return 0;

	}
	/**
	 * For the same note, the higher frets on the guitar fretboards are harder to play than the lower ones 
	 * Locality cost is sum of the fret numbers of the notes
	 * @return locality cost of an edge
	 */
	public double f1() { // locality
		int fretSum = guitarNodeSource.getGuitarPosition().getFret()
				+ guitarNodeDestination.getGuitarPosition().getFret();
		return fretSum / 6.8;
	}

	/**
	 * This penalty depends on the vertical distance between two fingering positions.
	 * If two notes are played from two different strings,
	 * two most convenient left hand fingers occurs when difference between fingering numbers and string numbers are equal.
	 * @return vertical stretch cost of an edge
	 */
	public double f2() { // across
		int deltaString = guitarNodeDestination.getGuitarPosition().getString()
				- guitarNodeSource.getGuitarPosition().getString();
		int source = guitarNodeSource.getGuitarPosition().getString();
		int deltaFinger = guitarNodeDestination.getFingeringNumber()
				- guitarNodeSource.getFingeringNumber();
		int fretDestination = guitarNodeDestination.getGuitarPosition()
				.getFret();
		int fretSource = guitarNodeSource.getGuitarPosition().getFret();
		if (source == 0||fretDestination==0||fretSource==0)
			return 0;
		if (deltaString == 0
				|| (deltaString == deltaFinger && deltaString * deltaFinger > 0))
			return 2.5;
		else
			return 5;
	}
	/**
	 * If a string is changed in transition from one note to another, 
	 * playing two notes with same left hand finger is very inconvenient and therefore should heavily be penalized.
	 * @return a cost penalty in case of same finger on different strings during consecutive notes
	 */
	public double f3() { // same finger on different strings
		int deltaFinger = guitarNodeDestination.getFingeringNumber()
				- guitarNodeSource.getFingeringNumber();
		int deltaString = guitarNodeDestination.getGuitarPosition().getString()
				- guitarNodeSource.getGuitarPosition().getString();
		int fretDestination = guitarNodeDestination.getGuitarPosition()
				.getFret();
		int fretSource = guitarNodeSource.getGuitarPosition().getFret();
		if (fretDestination==0||fretSource==0)
			return 0;
		if (deltaString != 0 && deltaFinger == 0)
			return 5.0;
		else
			return 0;
	}
	//favor fingering equal to fret number after playing open string
	/**
	 * Favor playing in first position after playing open string
	 * @return a low cost if next note is played in first position after open string, else return a high cost
	 */
	public double f4() {
		int fretDestination = guitarNodeDestination.getGuitarPosition()
				.getFret();
		int fingerDestination = guitarNodeDestination.getFingeringNumber();
		int fretSource = guitarNodeSource.getGuitarPosition().getFret();
		if (fretSource == 0 && fretDestination != 0) {
			if (fretDestination == fingerDestination)
				return 2.5;
			else
				return 5;
		}
		return 0;
	}
	/**
	 * Cost of sliding
	 * @return a penalty cost if edge represents a slide
	 */
	public double f5() {
		int fretDestination = guitarNodeDestination.getGuitarPosition()
				.getFret();
		int fretSource = guitarNodeSource.getGuitarPosition().getFret();
		int stringSource = guitarNodeSource.getGuitarPosition().getString();
		int stringDestination = guitarNodeDestination.getGuitarPosition().getString();
		if (fretDestination == 0 || fretSource == 0)
			return 0;
		int deltaFret = fretDestination - fretSource;
		int deltaFinger = guitarNodeDestination.getFingeringNumber()
				- guitarNodeSource.getFingeringNumber();
		int deltaString = stringDestination-stringSource;
		if (deltaString == 0  && deltaFret!=0&&deltaFinger == 0) {
			return slidingCost;
		} else
			return 0;
	}
	/*
	 * public double f4(){//Open Transition int deltaFret =
	 * guitarNodeDestination.getGuitarPosition().getFret() -
	 * guitarNodeSource.getGuitarPosition().getFret(); int deltaFinger =
	 * guitarNodeDestination.getFingeringNumber() -
	 * guitarNodeSource.getFingeringNumber(); int fretDestination =
	 * guitarNodeDestination.getGuitarPosition().getFret(); int fretSource =
	 * guitarNodeSource.getGuitarPosition().getFret();
	 * if(fretDestination==0||fretSource==0){ int absDeltaFret =
	 * Math.abs(deltaFret); int absDeltaFinger = Math.abs(deltaFinger);
	 * if(absDeltaFret<absDeltaFinger){ return 5; } else return 0; } return 0; }
	 */

}
