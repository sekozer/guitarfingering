/**
 * A single note parsed from midi file.
 * @author sekozer
 *
 */
public class MidiNote implements Comparable<MidiNote>{
	long start;
	long end;
	int key;
	public MidiNote(long start, long end, int key){
		this.start = start;
		this.end = end;
		this.key = key;
	}
	@Override
	public int compareTo(MidiNote o) {
		// TODO Auto-generated method stub
		int startComp = Long.valueOf(start).compareTo(o.start);
		return startComp!=0?startComp:Integer.valueOf(o.key).compareTo(key);
	}
	public String toString(){
		return "from " + start + " to "+ end+ " "+key;
	}
	public long getDuration(){
		return end-start;
	}
}
