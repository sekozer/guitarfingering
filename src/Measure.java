import java.util.ArrayList;
import java.util.List;
/**
 * Represents a measure in a musical score.It has array of notes, sign, clean, beats and beat type and a mode
 * @author sekozer
 *
 */
public class Measure {

	List<Note> notes = new ArrayList<Note>();
	private int beats=-1;
	private int beatType=-1;
	private int fifths=-1;
	private int line=-1;
	private char clef=0;
	private String sign="";
	private String mode="major";
	private int divisions;
	
	private Measure parentMeasure;
	
	public int getBeatType() {
		return beatType;
	}
	public void setBeatType(int beatType) {
		this.beatType = beatType;
	}
	public List<Note> getNotes() {
		return notes;
	}
	public void addNote(Note note){
		notes.add(note);
	}
	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}
	public int getBeats() {
		return beats;
	}
	public void setBeats(int beats) {
		this.beats = beats;
	}
	public int getFifths() {
		return fifths;
	}
	public void setFifths(int fifths) {
		this.fifths = fifths;
	}

	public int getClef() {
		return clef;
	}
	public void setClef(char clef) {
		this.clef = clef;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public int getDivisions() {
		return divisions;
	}
	public void setDivisions(int divisions) {
		this.divisions = divisions;
	}
	public int getLine() {
		return line;
	}
	public void setLine(int line) {
		this.line = line;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public Measure getParentMeasure() {
		return parentMeasure;
	}



}
