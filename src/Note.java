public class Note {
	public long duration;
	public long offset;
	public int noteNumber;
	public int octave;
	public int step;
	public static final String[] NOTE_NAMES = { "C", "C#", "D", "D#", "E", "F",
			"F#", "G", "G#", "A", "A#", "B" };
	public boolean isGhost = false;


	
	public Note(long duration, int octave, int step) {
		this.duration = duration;
		this.octave = octave;
		this.step = step;
		this.noteNumber = (octave + 1) * 12 + step;
	}

	public Note(long duration, int octave, String step) {
		this.duration = duration;
		this.octave = octave;
		this.step = findstep(step);
		this.noteNumber = (octave + 1) * 12 + this.step;
	}

	public Note(String duration, String octave, String step) {
		this.duration = Long.parseLong(duration);
		this.octave = Integer.parseInt(octave);
		this.step = findstep(step);
		this.noteNumber = (this.octave + 1) * 12 + this.step;
	}

	public int findstep(String step) {
		char letter = step.charAt(0);
		int diff = letter - 'C';
		if (step.length() == 2)
			diff++;
		diff = diff % 12;
		if (diff < 0)
			diff += 12;
		return diff;

	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getNoteNumber() {
		return noteNumber;
	}
	public double getQuarterLength(){
		return duration/8.0;
	}
	
	public static final int quarterLength = 8;
	public String getType() {

		if (duration == quarterLength*4)
			return "whole";
		else if (duration == quarterLength*2) {
			return "half";
		} else if (duration == quarterLength) {
			return "quarter";
		} else if (duration== quarterLength/2) {
			return "eighth";
		} else if (duration == quarterLength/4) {
			return "16th";
		} else if (duration == quarterLength/8) {
			return "32nd";
		}
		return null;
	}
	public int getTabWidth(){
		return (int) duration;
	}
	public boolean isRest(){
		return noteNumber==-1;
	}
	public long getOffset() {
		return offset;
	}

	public long getDuration() {
		return duration;
	}

	public String toString() {
		return "duration: " + duration + " Note: "
				+ getNoteName();
	}

	private String getNoteName() {
		return NOTE_NAMES[step] + " " + octave;
	}
	public static int getStep(String noteName){
		for (int i = 0; i < NOTE_NAMES.length; i++) {
			if(NOTE_NAMES[i].substring(0,1).equals(noteName))
				return i;
		}
		return -1;
	}
	public static String getLetter(int i){
		String name = NOTE_NAMES[i];
		return name.substring(0,1);
	}
	public static boolean  alter(int i){
		if(i==1||i==3||i==6||i==8||i==10)
			return true;
		return false;
	}

}
