import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;
/**	
 * MidiParser.java
 * purpose: Parsing a sequence of notes from midi file.
 * @author Serkan Ozer
 */
public class MidiParser extends Parser {

	public MidiParser(String fileName) {
		super(fileName);
		try {
			sequence = MidiSystem.getSequence(new File(fileName));
		} catch (InvalidMidiDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	Sequence sequence;

	public static final int NOTE_ON = 0x90;
	public static final int NOTE_OFF = 0x80;
	public static final int TIME_SIGNATURE = 0x58;
	public static final int KEY_SIGNATURE = 0x59;
	public static final int quarterLength = Note.quarterLength;
	/**
	 * Parse a note from midi number
	 * @param data midi number
	 * @param timeStamp timestamp of note
	 * @return
	 */
	public int parseNoteOn(int data, long timeStamp) {
		int key = data;
		int octave = (key / 12) - 1;
		int note = key % 12;
		String noteName = NOTE_NAMES[note];
		System.out.println("Note on, " + noteName + octave + " key=" + key);
		System.out.println();
		return key;
	}
	/**
	 * Parse a note from midi number
	 * @param data midi number
	 * @param timeStamp timestamp of note
	 * @return
	 */
	public int parseNoteOff(int data, long timeStamp) {
		int key = data;
		int octave = (key / 12) - 1;
		int note = key % 12;
		String noteName = NOTE_NAMES[note];
		System.out.println("Note of, " + noteName + octave + " key=" + key);
		System.out.println();
		return key;
	}
	/**
	 * Returns a rest note instance which
	 * @param restDuration duration of rest
	 * @param resolution 
	 * @return rest note
	 */
	public Note parseRest(double restDuration, long resolution) {
		double restNoteDuration = ((double) restDuration) / resolution;
		// System.out.println("rest" + restNoteDuration);
		if (restNoteDuration > 0.15) {
			Note restN = new Note(Math.round(quarterLength * restNoteDuration),
					-1, -1);

			return restN;

		}
		return null;

	}
	/**
	 * Returns a note instance
	 * @param key midi number of note
	 * @param duration duration of  note
	 * @param resolution number of midi ticks per quarter note
	 * @return rest note
	 */
	public Note parseNote(int key, long duration, long resolution) {
		double noteDuration = ((double) duration) / resolution;


		int octave = (key / 12) - 1;
		int step = key % 12;
		Note n = new Note(Math.round(quarterLength * noteDuration), octave,
				step);
		// parseNoteOn(key, 0);
		return n;
	}
	/**
	 * Parse a time signature(denominator and numerator)
	 * @param 4 byte data of midi time signature event
	 * @param Score instance to which time signature attached
	 */
	public void parseTimeSignature(byte data[], Score s) {
		int numerator = data[0] & 0xFF;
		s.setNumerator(numerator);
		int denominator = (int) Math.pow(data[1] & 0xFF, 2);
		s.setDenominator(denominator);
		int metronomePulse = data[2] & 0xFF;
		int numOf32nds = data[3] & 0xFF;
		String dataString = numerator + "/" + denominator + " metronome click "
				+ metronomePulse / 24 + " numOf32nds in a quarter note: "
				+ numOf32nds;
		//System.out.println(dataString);
	}
	/**
	 * Parse a key signature(denominator and numerator)
	 * @param  2 byte data of midi key signature event
	 * @param Score instance to which time signature attached
	 */
	public void parseKeySignature(byte data[], Score s) {
		int key = data[0] & 0xFF;
		s.setKeySignature(key);
		boolean scale = data[1] > 0;
		if (scale)
			s.setMajMin("minor");
		else
			s.setMajMin("major");
		String dataString = getKeyName(key, scale);
		//System.out.println(dataString);
	}
	/**
	 * Find and returns the track which includes musical midi events(note on - note off)
	 * @param tracks
	 * @return
	 */
	public Track findTrack(Track[] tracks) {
		for (Track track : tracks) {
			for (int i = 0; i < track.size(); i++) {
				MidiEvent event = track.get(i);
				MidiMessage message = event.getMessage();
				if (message instanceof ShortMessage) {
					ShortMessage sm = (ShortMessage) message;
					int command = sm.getCommand();
					if (command == NOTE_ON && sm.getData2() != 0) {
						return track;
					} else if ((command == NOTE_ON && sm.getData2() == 0)
							|| command == NOTE_OFF) {
						return track;
					}
				}

			}
		}
		return null;
	}

	public List<MidiNote> parseMidiNotes() {
		List<MidiNote> midiNotes = new ArrayList<MidiNote>();
		/*
		System.out.println("resolution " + sequence.getResolution());
		System.out.println("division " + sequence.getDivisionType());
		System.out.println("track # " + sequence.getTracks().length);
		*/
		long timeStamps[] = new long[129];
		Track track = findTrack(sequence.getTracks());

		for (int i = 0; i < track.size(); i++) {
			MidiEvent event = track.get(i);
			MidiMessage message = event.getMessage();
			long timeStamp = event.getTick();
			if (message instanceof ShortMessage) {
				ShortMessage sm = (ShortMessage) message;
				int command = sm.getCommand();
				if (command == NOTE_ON && sm.getData2() != 0) {
					int key = sm.getData1();
					timeStamps[key] = timeStamp;
				} else if ((command == NOTE_ON && sm.getData2() == 0)
						|| command == NOTE_OFF) {
					int key = sm.getData1();
					midiNotes
							.add(new MidiNote(timeStamps[key], timeStamp, key));
				}

			}

		}

		return midiNotes;
	}
	/**
	 * Filter polyphonic set of midinotes to make them monophonic
	 * @param midiNotes polpyhonic set of midi notes
	 * @return return monophonic midi note list
	 */
	public List<MidiNote> FilterMidiNotes(List<MidiNote> midiNotes) {
		Collections.sort(midiNotes);

		List<MidiNote> filteredMidiNotes = new ArrayList<MidiNote>();
		for (int i = 0; i < midiNotes.size(); i++) {
			MidiNote midiNote = midiNotes.get(i);
			filteredMidiNotes.add(midiNote);
			while (++i < midiNotes.size()) {
				MidiNote next = midiNotes.get(i);
				if (next.start >= midiNote.end) {
					i--;
					break;
				}

			}
		}

		return filteredMidiNotes;
	}

	
	/**
	 * returns list of midi numbers of the parsed notes.
	 */
	public List<Integer> getNotes() {
		List<MidiNote> midiNotes = parseMidiNotes();
		List<MidiNote> filteredNotes = FilterMidiNotes(midiNotes);
		List<Integer> midiNumbers = new ArrayList<Integer>();
		for (MidiNote midiNote : filteredNotes) {
			midiNumbers.add(midiNote.key);
		}
		return midiNumbers;
	}
	/**
	 * Converts midi to a score instance
	 */
	public Score getScore() {
		long resolution = sequence.getResolution();
		List<MidiNote> filteredNotes = FilterMidiNotes(parseMidiNotes());
		Score s = new Score();
		scoreAttributes(s);
		MidiNote midiNote = filteredNotes.get(0);
		Note n = parseNote(midiNote.key, midiNote.getDuration(), resolution);
		s.addNote(n);
		for (int i = 1; i < filteredNotes.size(); i++) {
			double restDuration = filteredNotes.get(i).start - midiNote.end;
			midiNote = filteredNotes.get(i);
			Note rest = parseRest(restDuration, resolution);
			if (rest != null) {
				s.addNote(rest);
			}
			n = parseNote(midiNote.key, midiNote.getDuration(), resolution);
			s.addNote(n);
		}

		return s;
	}
	
	public static final String noteNamesWithSharps[] = { "C", "C#", "D", "D#",
		"E", "F", "F#", "G", "G#", "A", "A#", "B" };
public static final String noteNamesWithFlats[] = { "C", "Db", "D", "Eb",
		"E", "F", "Gb", "G", "Ab", "A", "Bb", "B" };
/**
 * return a key name depending number of accidentals and is m
 * @param numOfAccidentals
 * @param isMinor
 * @return
 */
private String getKeyName(int numOfAccidentals, boolean isMinor) {

	short c1 = -5;

	String majMin = isMinor ? "minor" : "major";
	for (int i = 0; i < 12; i++) {
		int currentAccidentals = (c1 * i) % 12;
		if (!(Math.abs(currentAccidentals) < 6))
			currentAccidentals += 12;
		String tonicName = "";
		if (currentAccidentals == numOfAccidentals) {
			if (numOfAccidentals < 0)
				tonicName = noteNamesWithFlats[i];
			else {
				tonicName = noteNamesWithSharps[i];
			}
			return tonicName + " " + majMin;
		}

	}

	return null;

}
	/**
	 * Add key signature, time signature events from midi to a score instance
	 * @param s score s
	 */ 
	public void scoreAttributes(Score s) {
		Track tracks[] = sequence.getTracks();
		boolean isFinished = false;
		for (int trackNum = 0; trackNum < tracks.length; trackNum++) {
			Track track = tracks[trackNum];

			for (int i = 0; i < track.size(); i++) {
				MidiEvent event = track.get(i);
				MidiMessage message = event.getMessage();
				if (message instanceof MetaMessage) {
					MetaMessage mm = (MetaMessage) message;
					byte data[] = mm.getData();
					if (mm.getType() == TIME_SIGNATURE) {
						parseTimeSignature(data, s);
						if (isFinished)
							break;
						else
							isFinished = true;
					} else if (mm.getType() == KEY_SIGNATURE) {
						parseKeySignature(data, s);
						if (isFinished)
							break;
						else
							isFinished = true;
					}
				}
			}

		}
	}


}
