import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;



public class OptimalFingering {
	
	private List<GuitarNode> shortestPath = new ArrayList<GuitarNode>(); // tracks the nodes of shortest path form source to destination
	private Map<GuitarNode, Double> distance = new HashMap<GuitarNode, Double>();
	private Map<GuitarNode, GuitarNode> previousNodeMap = new HashMap<GuitarNode, GuitarNode>(); // when coming form source, this map keeps all nodes' previous nodes.
	
	
	
	Graph graph = new Graph();
	
	public OptimalFingering(Graph g){
		this.graph=g;
	}
	/**
	 * 
	 * @param nodes
	 * @return from source node to minimum distance node in the destination layer 
	 */
	public GuitarNode getMinDistanceNode(Set<GuitarNode> nodes){
		double minDistTemp=Double.MAX_VALUE;
		GuitarNode tempNode=null;
		for(GuitarNode gn: nodes){
			if(distance.get(gn) < minDistTemp){
				minDistTemp = distance.get(gn);
				tempNode = gn;
			}	
		}
		return tempNode;
	}
	/**
	 * 
	 * @param lastGuitarNode
	 * @return keeps track of the path from source to any given node
	 */
	public List<GuitarNode> deriveShorthestPath(GuitarNode lastGuitarNode){
		List<GuitarNode> reversePath = new ArrayList<GuitarNode>();
		List<GuitarNode> path = new ArrayList<GuitarNode>();
		GuitarNode previousGuitarNode;
		
		for(GuitarNode gn = lastGuitarNode; previousNodeMap.containsKey(gn); gn = previousGuitarNode ){
			reversePath.add(gn);
			previousGuitarNode = previousNodeMap.get(gn);	
		}
		for(int i = reversePath.size()-1; 0<=i; i--){
			path.add(reversePath.get(i));
			
		}
		return path;
	}
	/**
	 * this method finds the optimal path by running dijkstra s algorithm 
	 * @return
	 */
	public List<GuitarNode> DijkstrasAlgortihm ()  {
		Set<GuitarNode> unKnownNodes = new HashSet<GuitarNode>(); // Q queue of the remianing nodes
		Set<GuitarNode> knownNodes = new HashSet<GuitarNode>();
		GuitarNode currentNode;
		GuitarNode destinationNode;
		double alt;
		
		for(GuitarNode gn : graph.allVertex()){
			distance.put(gn, Double.MAX_VALUE );
		}
		distance.put(graph.getSource(), 0.0);
		
		unKnownNodes.add(graph.getSource());
		//currentNodes.remove(graph.getSource());
		
		while(!unKnownNodes.isEmpty()){
			currentNode = getMinDistanceNode(unKnownNodes);
			for(Edge edge : graph.getNeighbours(currentNode)){
				destinationNode = edge.getDestination();
				if(!knownNodes.contains(destinationNode)){
					unKnownNodes.add(destinationNode);
					alt = distance.get(currentNode) + edge.getCost();
					if(alt < distance.get(destinationNode)){
						distance.put(destinationNode, alt);
						previousNodeMap.put(destinationNode, currentNode);
					}
				}
				
			}
			unKnownNodes.remove(currentNode);
			knownNodes.add(currentNode);	
		}
		/*
		for (GuitarNode guitarNode : graph.getDestinationLayer()) {
			System.out.println(guitarNode.getDotString()+distance.get(guitarNode));
		}
		*/
		GuitarNode lastNodeOfShortestPath = getMinDistanceNode(new HashSet<GuitarNode>(graph.getDestinationLayer()));
		shortestPath = deriveShorthestPath(lastNodeOfShortestPath);
		
		return shortestPath; 
	}
	
}
