import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Parses MusicXML files
 * 
 * @author sekozer
 * 
 */
public class MusicXMLParser extends Parser {

	public MusicXMLParser(String fileName) {
		super(fileName);
		File fXmlFile = new File(fileName);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setValidating(false);
		dbFactory.setNamespaceAware(true);
		try {
			dbFactory.setFeature("http://xml.org/sax/features/namespaces",
					false);
			dbFactory.setFeature("http://xml.org/sax/features/validation",
					false);
			dbFactory
					.setFeature(
							"http://apache.org/xml/features/nonvalidating/load-dtd-grammar",
							false);
			dbFactory
					.setFeature(
							"http://apache.org/xml/features/nonvalidating/load-external-dtd",
							false);
		} catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(fXmlFile);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		doc.getDocumentElement().normalize();

		// TODO Auto-generated constructor stub
	}

	/**
	 * Parse MusicXML attribute element and attach parsed information to Measure
	 * element
	 * 
	 * @param m
	 * @param attributesElement
	 */
	public void parseAttributes(Measure m, Element attributesElement) {
		if (attributesElement != null) {
			Element divisionElement = (Element) attributesElement
					.getElementsByTagName("divisions").item(0);
			if (divisionElement != null) {
				String division = divisionElement.getTextContent();
				m.setDivisions(Integer.parseInt(division));
				parentMeasure.setDivisions(Integer.parseInt(division));
			}

			Element keyElement = (Element) attributesElement
					.getElementsByTagName("key").item(0);
			if (keyElement != null) {
				Element fifthElement = (Element) keyElement
						.getElementsByTagName("fifths").item(0);
				if (fifthElement != null) {
					String fifths = fifthElement.getTextContent();
					m.setFifths(Integer.parseInt(fifths));
				}

				Element modeElement = (Element) keyElement
						.getElementsByTagName("mode").item(0);

				if (modeElement != null) {
					String mode = modeElement.getTextContent();
					m.setMode(mode);
				}
			}

			Element timeElement = (Element) attributesElement
					.getElementsByTagName("time").item(0);
			if (timeElement != null) {
				Element beatsElement = (Element) timeElement
						.getElementsByTagName("beats").item(0);
				if (beatsElement != null) {
					String beats = beatsElement.getTextContent();
					m.setBeats(Integer.parseInt(beats));
				}

				Element beatTypeElement = (Element) timeElement
						.getElementsByTagName("beat-type").item(0);
				if (beatTypeElement != null) {
					String beatType = beatTypeElement.getTextContent();
					m.setBeatType(Integer.parseInt(beatType));
				}
			}
			Element clefElement = (Element) attributesElement
					.getElementsByTagName("clef").item(0);
			Element signElement = (Element) clefElement.getElementsByTagName(
					"sign").item(0);
			if (signElement != null) {
				String sign = signElement.getTextContent();
				m.setSign(sign);
			}
			Element lineElement = (Element) clefElement.getElementsByTagName(
					"line").item(0);
			if (lineElement != null) {
				String line = lineElement.getTextContent();
				m.setLine(Integer.parseInt(line));
			}
		}

	}

	/**
	 * Parse a note instance from MusicXML note element
	 * 
	 * @param noteElement
	 * @param divisions
	 *            MusicXML division value
	 * @return
	 */
	public Note parseNote(Element noteElement, int divisions) {
		NodeList pitchs = noteElement.getElementsByTagName("pitch");
		String durationString = getFirstText(noteElement
				.getElementsByTagName("duration"));

		long duration = Long.parseLong(durationString);
		long noteDuration = (divisions / duration) * 8;
		if (pitchs.getLength() > 0) {

			Element pitchElement = (Element) pitchs.item(0);
			Element tieElem =(Element) pitchElement.getElementsByTagName("tie").item(0);
			
			String step = getFirstText(pitchElement
					.getElementsByTagName("step"));
			String octave = getFirstText(pitchElement
					.getElementsByTagName("octave"));
			Element alterElem =(Element) pitchElement.getElementsByTagName("alter").item(0);
			int stepNum = Note.getStep(step);

			if(alterElem!=null){
				String alterString = alterElem.getTextContent();
				int alter =Integer.parseInt(alterString);
				stepNum+=alter;
			}
			Note note = new Note(noteDuration, Integer.parseInt(octave),
					stepNum);
			if(tieElem!=null&&tieElem.getAttribute("type").equals("end"))
				note.isGhost=true;

			return note;
		} else {
			Note note = new Note(noteDuration, -1, -1);
			return note;
		}

	}

	private String getFirstText(NodeList nList) {
		return nList.item(0).getTextContent();
	}

	private Document doc;
	private Measure parentMeasure = new Measure();

	/**
	 * Return list of parsed notes
	 */
	@Override
	public List<Integer> getNotes() {
		List<Integer> midiNotes = new ArrayList<Integer>();
		NodeList nList = doc.getElementsByTagName("measure");
		for (int i = 0; i < nList.getLength(); i++) {
			Node measure = nList.item(i);
			if (measure.getNodeType() == Node.ELEMENT_NODE) {

				Element measureElement = (Element) measure;
				Measure m = new Measure();
				parseAttributes(m, (Element) measureElement
						.getElementsByTagName("attributes").item(0));
				NodeList notes = measureElement.getElementsByTagName("note");
				for (int j = 0; j < notes.getLength(); j++) {
					Element noteElement = (Element) notes.item(j);
					Note n = parseNote(noteElement,
							parentMeasure.getDivisions());
					if(!n.isRest())
					midiNotes.add(n.getNoteNumber());
				}

			}
		}
		return midiNotes;
	}

	/**
	 * Parse and returns Score instance from MusicXML file
	 */
	@Override
	public Score getScore() {
		// TODO Auto-generated method stub
		Score s = new Score();
		NodeList nList = doc.getElementsByTagName("measure");
		for (int i = 0; i < nList.getLength(); i++) {
			Node measure = nList.item(i);
			if (measure.getNodeType() == Node.ELEMENT_NODE) {

				Element measureElement = (Element) measure;

				System.out.println("measure id : "
						+ measureElement.getAttribute("number"));
				Measure m = new Measure();
				parseAttributes(m, (Element) measureElement
						.getElementsByTagName("attributes").item(0));
				NodeList notes = measureElement.getElementsByTagName("note");
				for (int j = 0; j < notes.getLength(); j++) {	
					Element noteElement = (Element) notes.item(j);
					Note n = parseNote(noteElement, m.getDivisions());
					m.addNote(n);
				}
				s.addMeasure(m);

			}
		}
		return s;
	}

}
