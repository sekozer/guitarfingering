public class GuitarNode {
	/**
	 * Instances of this class represent a fingering on guitar fretboard.
	 */
	private GuitarPosition guitarPosition;
	private int fingeringNumber;
	int layerIndex = 0;

	public void setLayerIndex(int layerIndex) {
		this.layerIndex = layerIndex;
	}
	/**
	 * @param guitarPosition Guitar position (string,fret) of a played note
	 * @param fingeringNumber fingerin number of a played note
	 */
	public GuitarNode(GuitarPosition guitarPosition, int fingeringNumber) {
		this.guitarPosition = guitarPosition;
		this.fingeringNumber = fingeringNumber;
	}
	/**
	 * Creates a guitar node
	 * @param string string number of the played note
	 * @param fret	fret number of the played note
	 * @param fingeringNumber left hand finger number of played note
	 */
	public GuitarNode(int string,int fret, int fingeringNumber) {
		this.guitarPosition = new GuitarPosition(string, fret);
		this.fingeringNumber = fingeringNumber;
	}
	/**
	 * @return dot string of a guitar node
	 */
	public String getDotString() {

		int fret = guitarPosition.getFret();
		int string = guitarPosition.getString();
		return "\"<" + string + "," + fret + "," + fingeringNumber/*
																 * +","+layerIndex
																 */+ ">\"";
	}

	public String toString() {
		int fret = guitarPosition.getFret();
		int string = guitarPosition.getString();
		return "\"<" + string + "," + fret + "," + fingeringNumber + ">\"";
	}
	/**
	 * @return fingering number of a guitar node
	 */
	public int getFingeringNumber() {
		return fingeringNumber;
	}
	/**
	 * 
	 * @return Guitar position of a guitar node
	 */
	public GuitarPosition getGuitarPosition() {
		return this.guitarPosition;
	}
	/**
	 * Check if two guitar node are equal(their string,fret and fingering numbers are equal)
	 * @param guitarNode compared node
	 * @return true if two guitar node is equal
	 */
	public boolean equal(GuitarNode guitarNode) {
		if (guitarNode.getGuitarPosition().getFret() == this
				.getGuitarPosition().getFret()
				&& 
				guitarNode.getGuitarPosition().getString() == this
				.getGuitarPosition().getString()
				&& guitarNode.getFingeringNumber() == this.getFingeringNumber())
			return true;
		return false;
	}
	/**
	 * 
	 * @return corresponding midi number of a guitar node
	 */
	public int getMidiNote() {
		int fret = guitarPosition.getFret();
		int string = guitarPosition.getString();
		int midiNote = 40 + (6 - string) * 5 + fret;
		if (string < 3)
			midiNote--;
		return midiNote;
	}

}
