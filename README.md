### What is this repository for? ###

 An implementation of a guitar finding algorithm which maps fingerings(which fret and with which left hand finger a note should be played) to a given given score. It can input MusicXML and MIDI and output in MusicXML and ASCII tab